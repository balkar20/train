﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwaitReflect
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("MainTreadId {0}", Thread.CurrentThread.ManagedThreadId);
            MyClass my = new MyClass();
            my.OperationAsync();

            Console.ReadKey();
        }
    }

    class MyClass
    {
        public void Operation()
        {
            Console.WriteLine("Operation ThreadId {0}", Thread.CurrentThread.ManagedThreadId);
            Console.WriteLine("Begin");
            Thread.Sleep(2000);
            Console.WriteLine("End");
        }

        public void OperationAsync()
        {
            MyClass.AsyncStateMachine stateMachine;
            stateMachine.outer = this;
            stateMachine.builder = AsyncVoidMethodBuilder.Create();
            //Метод Start() вызывает метод MoveNext()
            stateMachine.builder.Start<MyClass.AsyncStateMachine>(ref stateMachine);
        }

        [CompilerGenerated]
        [StructLayout(LayoutKind.Auto)]
        private struct AsyncStateMachine:IAsyncStateMachine
        {
            public AsyncVoidMethodBuilder builder;
            public MyClass outer;

            void IAsyncStateMachine.MoveNext()
            {
                Task task = new Task(outer.Operation);
                task.Start();
            }

            void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
            {
                //////
            }
        }
    }
}
