﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwaitReturnValueAndinputArgs
{
    class Program
    {
        static void Main(string[] args)
        {
            MyClass my = new MyClass();
            Task<double> task = my.OperationAsync(3);

            task.ContinueWith(t => Console.WriteLine("Результаьт: {0}", t.Result));

            Console.ReadKey();
        }
    }

    class MyClass
    {
        double Operation(object argument)
        {
            Thread.Sleep(2000);
            return (double)argument * (double)argument;
        }

        public async Task<double> OperationAsync(double argument)
        {
            //int result = await task<int>.Factory.StartNew(Operation);
            //return result
            return await Task<double>.Factory.StartNew(Operation, argument);
        }
    }
}
