﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwait2ReflectTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("MainTreadId {0}", Thread.CurrentThread.ManagedThreadId);
            MyClass my = new MyClass();
            my.OperationAsync();

            Console.ReadKey();
        }
    }

    class MyClass
    {
        public void Operation()
        {
            Console.WriteLine("Operation ThreadId {0}", Thread.CurrentThread.ManagedThreadId);
            Console.WriteLine("Begin");
            Thread.Sleep(2000);
            Console.WriteLine("End");
        }

        public void OperationAsync()
        {
            AsyncStateMachine stateMachine;
            stateMachine.outer = this;
            stateMachine.builder = AsyncVoidMethodBuilder.Create();
            stateMachine.state = -1;
            //Метод Start() вызывает метод MoveNext()
            stateMachine.builder.Start(ref stateMachine);
        }
        public int counterCallMoveNext =0;
        private struct AsyncStateMachine : IAsyncStateMachine
        {
            public AsyncVoidMethodBuilder builder;
            public MyClass outer;
            public int state;

            void IAsyncStateMachine.MoveNext()
            {
                Console.WriteLine("\n Method MoveNext was called {0}th tim,e in thread {1}", ++outer.counterCallMoveNext, Thread.CurrentThread.ManagedThreadId);

                if (state == -1)
                {
                    Console.WriteLine("OperationAsync(Part1) ThreadId {0}\n", Thread.CurrentThread.ManagedThreadId);
                    Task task = new Task(outer.Operation);
                    task.Start();

                    state = 0;
                    TaskAwaiter awaiter = task.GetAwaiter();
                    //Здесь метод MoveNext вызыфвается второй раз во вторичном потоке
                    builder.AwaitOnCompleted(ref awaiter, ref this);//Закоментировать- Он также вызывает setStateMachine
                    return;
                }
                Console.WriteLine("OperationAsync(Part2) ThreadId {0}\n", Thread.CurrentThread.ManagedThreadId);
            }

            void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
            {
                Console.WriteLine("\n Method SetStateMachine ThreadId {0}", Thread.CurrentThread.ManagedThreadId);
                Console.WriteLine("SetStateMachine.GetHashCode {0}", stateMachine.GetHashCode());
                Console.WriteLine("\n this.GetHashCode {0}\n", this.GetHashCode());
        
                builder.SetStateMachine(stateMachine);
            }
        }


    }
}
