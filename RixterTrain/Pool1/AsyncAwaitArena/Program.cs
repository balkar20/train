﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwaitArena
{
    class Program
    {
        static void Main(string[] args)
        {
            MyClass my = new MyClass();
            Task<double> task = my.OperationAsync(3);

            task.ContinueWith(t => Console.WriteLine("Результаьт: {0}", t.Result));

            Console.ReadKey();
        }
    }

    class MyClass
    {
        int Operation()
        {
            Thread.Sleep(2000);
            return 5*2;
        }

        public async Task<double> OperationAsync(double argument)
        {
            int result = await Task<int>.Factory.StartNew(Operation);

            result = await Task<int>.Factory.StartNew(Operation);

            return result;
        }
    }
}
