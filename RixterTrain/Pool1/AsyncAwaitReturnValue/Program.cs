﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwaitReturnValue
{
    class Program
    {
        static void Main(string[] args)
        {
            MyClass my = new MyClass();
            Task<int> task = my.OperationAsync();

            task.ContinueWith(t => Console.WriteLine("Результаьт: {0}", t.Result));

            Console.ReadKey();
        }
    }

    class MyClass
    {
        public int Operation()
        {
            Console.WriteLine("Operation ThreadId {0}", Thread.CurrentThread.ManagedThreadId);
            Thread.Sleep(2000);
            return 2 + 2;
        }

        public async Task<int> OperationAsync()
        {
            //int result = await task<int>.Factory.StartNew(Operation);
            //return result
            return await Task<int>.Factory.StartNew(Operation);
        }
    }
}
