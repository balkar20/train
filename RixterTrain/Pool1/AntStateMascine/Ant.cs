﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntStateMascine
{
    public class Ant
    {
        public BitVector32 position { get; set; }
        public BitVector32 velocity { get; set; }
        public BitVector32 poon { get; set; }

    }
}
