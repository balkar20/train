﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwaitTaskRetValue2Reflect
{
    class Program
    {
        static void Main(string[] args)
        {
            MyClass my = new MyClass();
            my.OperationAsync();

            Console.WriteLine("Первичный поток завершил работу. threadId {0}", Thread.CurrentThread.ManagedThreadId);

            Console.ReadKey();
        }
    }

    class MyClass
    {
        public int Operation()
        {
            Console.WriteLine("Operation ThreadId {0}", Thread.CurrentThread.ManagedThreadId);
            Thread.Sleep(2000);
            return 2 + 2;
        }

        public  void OperationAsync()
        {
            AsyncStateMachine stateMachine;
            stateMachine.outer = this;
            stateMachine.builder = AsyncVoidMethodBuilder.Create();
            stateMachine.state = -1;
            //Метод Start() вызывает метод MoveNext()
            stateMachine.builder.Start(ref stateMachine);
        }

        struct AsyncStateMachine : IAsyncStateMachine
        {
            public AsyncVoidMethodBuilder builder;
            public MyClass outer;
            public int state;
            private TaskAwaiter<int> awaiter;

            public void MoveNext()
            {
                if (state ==-1)
                {
                    Func<int> function = outer.Operation;
                    Task<int> task = Task<int>.Factory.StartNew(function);

                    state = 0;
                    awaiter = task.GetAwaiter();

                    builder.AwaitOnCompleted(ref awaiter, ref this);
                    return;
                }

                //Вторая половина метода во виоричном поттоке
                int result = awaiter.GetResult();
                Console.WriteLine("\nРезультат: {0}\n");
            }

            public void SetStateMachine(IAsyncStateMachine stateMachine)
            {
                builder.SetStateMachine(stateMachine);
            }
        }

       
   
    }
}
