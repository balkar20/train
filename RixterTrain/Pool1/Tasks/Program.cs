﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            var t = Task.Run(async delegate
            {
                await Task.Delay(10000);
                return 42;
            });
            t.Wait();
            Console.WriteLine("Task t Status: {0}, Result: {1}",
                              t.Status, t.Result);
        }

        private static Int32 Sum(Int32 n)
        {
            Int32 sum = 0;
            Thread.Sleep(1000);
            //for (; n > 0; n--)
            //    checked { sum += n; } // при больших n выдается System.OverflowException
            //return sum;
            return 13;
        }
    }
}
