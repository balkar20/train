﻿namespace ConActo2
{
    public abstract class GamePersonage
    {
        public string Name { get; set; }
        public int X { get; }
        public int Y { get; }

        protected GamePersonage(string name, int x, int y)
        {
            Name = name;
            X = x;
            Y = y;
        }
    }
}