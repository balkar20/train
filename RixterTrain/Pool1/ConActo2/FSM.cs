﻿using System;

namespace ConActo2
{
    public class FSM
    {
        public Action act;

        public FSM()
        {

        }

        public void SetState(Action state)
        {
            this.act = state;
        }

        public void Update()
        {
            if (act != null)
            {
                act();
            }
        }
    }
}