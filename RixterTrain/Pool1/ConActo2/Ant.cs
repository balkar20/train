﻿using System;
using System.Collections.Specialized;

namespace ConActo2
{
    public class Ant
    {
        public Vector3D position { get; set; }
        public Vector3D velocity { get; set; }
        public FSM brain { get; set; }

        Game game = new Game(new Instance(6, 7), new Instance(6,8));

        public Ant(Vector3D position, Vector3D velocity, FSM brain)
        {
            this.position = position;
            this.velocity = velocity;
            this.brain = brain;

            brain.SetState(FindLeaf);
        }

        public void FindLeaf()
        {
            velocity = new Vector3D(game.Leaf.X - position.X, game.Home.Y - position.Y);




        }

        public void GoHome()
        {

        }

        public void RunAvay()
        {

        }

        public void Update()
        {
            brain.Update();
        }
    }
}