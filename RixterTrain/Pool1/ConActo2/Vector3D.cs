﻿namespace ConActo2
{
    public class Vector3D
    {
        public int Y { get; }
        public int X { get; }

        public Vector3D(int y, int x)
        {
            Y = y;
            X = x;
        }
    }
}