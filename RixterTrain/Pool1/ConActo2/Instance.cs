﻿namespace ConActo2
{
    public class Instance
    {
        public int X { get; }
        public int Y { get; }

        public Instance(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}