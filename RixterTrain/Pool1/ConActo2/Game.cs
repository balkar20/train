﻿namespace ConActo2
{
    public class Game
    {
        public Instance Leaf { get; set; }
        public Instance Home { get; set; }
        public GamePersonage Mouse { get; set; }
        public GamePersonage Ant;

        public Game(Instance leaf, Instance home)
        {
            Leaf = leaf;
            Home = home;
        }
    }
}