﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwait2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("MainTreadId {0}", Thread.CurrentThread.ManagedThreadId);
            MyClass my = new MyClass();
            my.OperationAsync();

            Console.ReadKey();
        }
    }

    class MyClass
    {
        public void Operation()
        {
            Console.WriteLine("Operation ThreadId {0}", Thread.CurrentThread.ManagedThreadId);
            Console.WriteLine("Begin");
            Thread.Sleep(2000);
            Console.WriteLine("End");
        }

        public async void OperationAsync()
        {
            Console.WriteLine("OperationAsync(Part1) ThreadId {0}\n", Thread.CurrentThread.ManagedThreadId);
            Task task = new Task(Operation);
            task.Start();
            await task;
            Console.WriteLine("OperationAsync(Part2) ThreadId {0}\n", Thread.CurrentThread.ManagedThreadId);
        }

        
    }
}
