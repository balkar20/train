﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfAsync
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool flag = true;
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void ButtonPress_OnClick(object sender, RoutedEventArgs e)
        {
            await Task.Factory.StartNew(Do);
            Task task = new Task(Do);
            TaskScheduler she = TaskScheduler.Current;
        }

        private void ButtonLook_OnClick(object sender, RoutedEventArgs e)
        {
            LabelShow.Content = "EEEE";
            if (flag)
            {
                LabelShow.Background = new SolidColorBrush(Color.FromRgb(45, 54,58));
                flag = !flag;
            }
            else
            {
                LabelShow.Background = new SolidColorBrush(Color.FromRgb(12, 90, 30));
                flag = !flag;
            }
            
        }

        void Do()
        {
            Thread.Sleep(10000);
            MessageBox.Show("Comleated");
        }
    }
}
