﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwait2Reflect
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("MainTreadId {0}", Thread.CurrentThread.ManagedThreadId);
            MyClass my = new MyClass();
            my.OperationAsync();
            Console.WriteLine("I doing!");
            Console.ReadKey();
        }
    }
    class MyClass
    {
        public void Operation()
        {
            Console.WriteLine("Operation ThreadId {0}", Thread.CurrentThread.ManagedThreadId);
            Console.WriteLine("Begin");
            Thread.Sleep(2000);
            Console.WriteLine("End");
        }

        public void OperationAsync()
        {
            MyClass.AsyncStateMachine stateMachine;
            stateMachine.outer = this;
            stateMachine.builder = AsyncVoidMethodBuilder.Create();
            stateMachine.state = -1;
            //Метод Start() вызывает метод MoveNext()
            stateMachine.builder.Start<MyClass.AsyncStateMachine>(ref stateMachine);


        }

        [CompilerGenerated]
        [StructLayout(LayoutKind.Auto)]
        private struct AsyncStateMachine : IAsyncStateMachine
        {
            public AsyncVoidMethodBuilder builder;
            public MyClass outer;
            public int state;

            void IAsyncStateMachine.MoveNext()
            {
                if (state == -1)
                {
                    Console.WriteLine("OperationAsync(Part1) ThreadId {0}\n", Thread.CurrentThread.ManagedThreadId);
                    Task task = new Task(outer.Operation);
                    task.Start();

                    state = 0;
                    TaskAwaiter awaiter = task.GetAwaiter();
                    //Здесь метод MoveNext вызыфвается второй раз во вторичном потоке
                    builder.AwaitOnCompleted(ref awaiter, ref this);
                    return;
                }
                Console.WriteLine("OperationAsync(Part2) ThreadId {0}\n", Thread.CurrentThread.ManagedThreadId);
            }

            void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
            {
                builder.SetStateMachine(stateMachine);
            }
        }


    }
}
