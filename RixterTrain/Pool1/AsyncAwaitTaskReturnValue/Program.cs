﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwaitTaskReturnValue
{
    class Program
    {
        static void Main(string[] args)
        {
            MyClass my = new MyClass();
            my.OperationAsync();

            Console.WriteLine("Первичный поток завершил работу. threadId {0}", Thread.CurrentThread.ManagedThreadId);

            Console.ReadKey();
        }
    }

    class MyClass
    {
        public int Operation()
        {
            Console.WriteLine("Operation ThreadId {0}", Thread.CurrentThread.ManagedThreadId);
            Thread.Sleep(2000);
            return 2 + 2;
        }

        public void OperationAsync()
        {
            Task<int> task = Task<int>.Factory.StartNew(Operation);
            TaskAwaiter<int> awaiter = task.GetAwaiter();
            Action continuation = () =>
            {
                Console.WriteLine("\n Результат: {0}", awaiter.GetResult());
                Console.WriteLine("\n ThreadId {0}", Thread.CurrentThread.ManagedThreadId);
            };
            awaiter.OnCompleted(continuation);
        }


    }
}
