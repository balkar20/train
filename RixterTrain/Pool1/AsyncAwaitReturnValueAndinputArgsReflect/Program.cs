﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwaitReturnValueAndinputArgsReflect
{
    class Program
    {
        static void Main(string[] args)
        {
            MyClass my = new MyClass();
            Task<double> task = my.OperationAsync(3);

            task.ContinueWith(t => Console.WriteLine("Результаьт: {0}", t.Result));

            Console.ReadKey();
        }
    }

    class MyClass
    {
        double Operation(object argument)
        {
            Thread.Sleep(2000);
            return (double)argument * (double)argument;
        }

        public Task<double> OperationAsync(double argument)
        {
            AsyncStateMachine stateMachine;
            stateMachine.outer = this;
            stateMachine.builder = AsyncTaskMethodBuilder<double>.Create();
            stateMachine.state = -1;
            stateMachine.argument = argument;
            //Метод Start() вызывает метод MoveNext()
            stateMachine.builder.Start(ref stateMachine);

            return stateMachine.builder.Task;
        }

        struct AsyncStateMachine : IAsyncStateMachine
        {
            //public AsyncVoidMethodBuilder builder;//Для void
            public AsyncTaskMethodBuilder<double> builder; //для task
            public MyClass outer;
            public double argument;
            public int state;

            private TaskAwaiter<double> awaiter;

            public void MoveNext()
            {
                if (state == -1)
                {
                    Func<object, double> function = outer.Operation;
                    Task<double> task = Task<double>.Factory.StartNew(function, argument);
                    awaiter = task.GetAwaiter();
                    state = 0;

                    builder.AwaitOnCompleted(ref awaiter, ref this);
                    return;
                }

                //Задача помечается как успешно выполненнаяб 
                //тогда срабатывает продолжение
                double result = awaiter.GetResult();
                builder.SetResult(result);
            }

            public void SetStateMachine(IAsyncStateMachine stateMachine)
            {
                builder.SetStateMachine(stateMachine);
            }
        }
    }
}