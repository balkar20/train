﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwaitReturnValueReflector
{
    class Program
    {
        static void Main(string[] args)
        {
            MyClass my = new MyClass();
            Task<int> task = my.OperationAsync();

            task.ContinueWith(t => Console.WriteLine("Результаьт: {0}", t.Result));

            Console.ReadKey();
        }
    }

    class MyClass
    {
        public int Operation()
        {
            Console.WriteLine("Operation ThreadId {0}", Thread.CurrentThread.ManagedThreadId);
            Thread.Sleep(2000);
            return 2 + 2;
        }

        public Task<int> OperationAsync()
        {
            AsyncStateMachine stateMachine;
            stateMachine.outer = this;
            stateMachine.builder = AsyncTaskMethodBuilder<int>.Create();
            stateMachine.state = -1;
            //Метод Start() вызывает метод MoveNext()
            stateMachine.builder.Start(ref stateMachine);

            return stateMachine.builder.Task;
        }

        struct AsyncStateMachine : IAsyncStateMachine
        {
            //public AsyncVoidMethodBuilder builder;//Для void
            public AsyncTaskMethodBuilder<int> builder;//для task
            public MyClass outer;
            public int state;
            private TaskAwaiter<int> awaiter;

            public void MoveNext()
            {
                if (state == -1)
                {
                    Func<int> function = outer.Operation;
                    Task<int> task = Task<int>.Factory.StartNew(function);
                    awaiter = task.GetAwaiter();
                    state= 0;

                    builder.AwaitOnCompleted(ref awaiter, ref this);
                    return;
                }
                //Задача помечается как успешно выполненнаяб 
                //тогда срабатывает продолжение
                int result = awaiter.GetResult();
                builder.SetResult(result);
            }

            public void SetStateMachine(IAsyncStateMachine stateMachine)
            {
                builder.SetStateMachine(stateMachine);
            }
        }
    }
}
