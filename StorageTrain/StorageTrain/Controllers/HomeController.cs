﻿using System.Web.Mvc;

namespace CookieSample.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Users = "Vlad")]
        public ActionResult ManageAcc()
        {
            ViewBag.Message = "Secured Area";

            return View();
        }
    }
}