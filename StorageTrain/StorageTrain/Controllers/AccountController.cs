﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace StorageTrain.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            
            return View();
        }

        public ActionResult Login()
        {
            string userName = null;
            if (User.Identity.IsAuthenticated == true)
            {
                userName = User.Identity.Name;
            }

            ViewBag.username = userName;
            return View();
        }

        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            var passwordFromDB = "1000:x4";
            
            FormsAuthentication.SetAuthCookie(username, true);

            return Redirect(FormsAuthentication.GetRedirectUrl(username, false));
        }
    }
}