﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Routing.Constraints;

namespace StorageTrain.Filters
{
    public class MyAuthorizationAttribute:AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var user = httpContext.User.Identity.IsAuthenticated ? httpContext.User.Identity.Name : null;
            if (user != null)
            {
                
            }
            return false;
        }
    }
}