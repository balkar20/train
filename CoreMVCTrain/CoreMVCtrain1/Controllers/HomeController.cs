﻿using Microsoft.AspNetCore.Mvc;

namespace CoreMVCtrain1.Controllers
{
    public class HomeController : Controller
    {
        // GET
        public IActionResult Index()
        {
            return View();
        }
    }
}