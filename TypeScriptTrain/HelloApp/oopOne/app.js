var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var User = (function () {
    function User(userName) {
        this.name = userName;
    }
    User.prototype.getInfo = function () {
        console.log("Имя: " + this.name);
    };
    return User;
}());
var Employee = (function (_super) {
    __extends(Employee, _super);
    function Employee(userName, empCompany) {
        var _this = _super.call(this, userName) || this;
        _this.company = empCompany;
        return _this;
    }
    Employee.prototype.getInfo = function () {
        _super.prototype.getInfo.call(this);
        console.log("Работает в компании: " + this.company);
    };
    return Employee;
}(User));
var employee = {
    id: 1,
    name: "Tom"
};
console.log("id: " + employee.id);
console.log("name: " + employee.name);
var emp = new Employee("Vlad", "Artezo");
emp.getInfo();
//# sourceMappingURL=app.js.map