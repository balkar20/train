﻿class User {

    name: string;
    constructor(userName: string) {

        this.name = userName;
    }
    getInfo(): void {
        console.log("Имя: " + this.name);
    }
}

class Employee extends User {

    company: string;
    constructor(userName: string, empCompany: string) {

        super(userName);
        this.company = empCompany;
    }

    getInfo(): void {
        super.getInfo();
        console.log("Работает в компании: " + this.company);
    }
    
}

interface IUser {
    id: number;
    name: string;
}

let employee: IUser = {
     
    id: 1, 
    name: "Tom"
}
console.log("id: " + employee.id);
console.log("name: " + employee.name);

let emp: User = new Employee("Vlad", "Artezo");

emp.getInfo();
