﻿var t: any = true;
var z: string = "Hello";
t = "";
var slovo: string = `${z} World`;
var list: string[] = ["red", "green", "blue"];
let names: Array<string> = ["Tom", "Bob", "Alice"];

// определение кортежа - кортеж состоит из двух элементов - строки и числа
let userInfo: [string, number];
// инициализация кортежа
userInfo = ["Tom", 28];
// Неправильная инициализация - переданные значения не соответствуют типам по позиции
//userInfo = [28, "Tom"]; // Ошибка

// использование кортежа
console.log(userInfo[1]); // 28
userInfo[1] = 37;
class User {
    name: string;
    age: number;
    constructor(_name: string, _age: number) {

        this.name = _name;
        this.age = _age;
    }
}
var tom: User = new User("Том", 29);
console.log("Имя: ", tom.name, " возраст: ", tom.age);