var t = true;
var z = "Hello";
t = "";
var slovo = z + " World";
var list = ["red", "green", "blue"];
var names = ["Tom", "Bob", "Alice"];
// определение кортежа - кортеж состоит из двух элементов - строки и числа
var userInfo;
// инициализация кортежа
userInfo = ["Tom", 28];
// Неправильная инициализация - переданные значения не соответствуют типам по позиции
//userInfo = [28, "Tom"]; // Ошибка
// использование кортежа
console.log(userInfo[1]); // 28
userInfo[1] = 37;
var User = (function () {
    function User(_name, _age) {
        this.name = _name;
        this.age = _age;
    }
    return User;
}());
var tom = new User("Том", 29);
console.log("Имя: ", tom.name, " возраст: ", tom.age);
//# sourceMappingURL=app.js.map