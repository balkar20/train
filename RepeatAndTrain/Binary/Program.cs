﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Binary
{


    class Program
    {
        static int BSearch(int[] numbers, int value)
        {
            int size = numbers.Length;
            int low= 0;
            int high = size - 1;

            while (low <= high)
            {
                int mid = (low + high) / 2;
                if(value == numbers[mid])
                {
                    Console.WriteLine("Value  "+ value + " is located of " + mid );
                    return 0;
                }else if(value > numbers[mid])
                {
                    low = mid + 1;
                }
                else
                {
                    high = mid - 1;
                }
            }

            return 1;
        }


        static void Main(string[] args)
        {
            int[] numbers = new int[10] { 1,2,3,4,5,6,7,8,9,10};
            BSearch(numbers,3);
            
        }
    }
}
