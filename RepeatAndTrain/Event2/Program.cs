﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event2
{
    class AccountEventArgs
    {
        // Сообщение
        public string message;
        // Сумма, на которую изменился счет
        public int sum;

        public AccountEventArgs(string _mes, int _sum)
        {
            message = _mes;
            sum = _sum;
        }
    }

    class Account
    {
        // Объявляем делегат
        public delegate void AccountStateHandler(object sender, AccountEventArgs e);
        // Событие, возникающее при выводе денег
        public event AccountStateHandler Withdrowed;
        // Событие, возникающее при добавлении на счет
        public event AccountStateHandler Added;

        int _sum; // Переменная для хранения суммы
        int _percentage; // Переменная для хранения процента

        public Account(int sum, int percentage)
        {
            _sum = sum;
            _percentage = percentage;
        }

        public int CurrentSum
        {
            get { return _sum; }
        }

        public void Put(int sum)
        {
            _sum += sum;
            if (Added != null)
                Added(this, new AccountEventArgs("На счет поступило " + sum, sum));
        }
        public void Withdraw(int sum)
        {
            if (sum <= _sum)
            {
                _sum -= sum;
                if (Withdrowed != null)
                    Withdrowed(this, new AccountEventArgs("Сумма " + sum + " снята со счета", sum));
            }
            else
            {
                if (Withdrowed != null)
                    Withdrowed(this, new AccountEventArgs("Недостаточно денег на счете", sum));
            }
        }

        public int Percentage
        {
            get { return _percentage; }
        }
    }

    class Program
    {
        
        static void Main(string[] args)
        {
            Account account = new Account(200, 6);
            // Добавляем обработчики события
            account.Added += Show_Message;
            account.Withdrowed += Show_Message;

            account.Withdraw(100);
            // Удаляем обработчик события
            account.Withdrowed -= Show_Message;

            account.Withdraw(50);
            account.Put(150);

            Console.ReadLine();
        }
        private static void Show_Message(object sender, AccountEventArgs e)
        {
            Console.WriteLine("Сумма транзакции: {0}", e.sum);
            Console.WriteLine(e.message);
        }
    }
}
