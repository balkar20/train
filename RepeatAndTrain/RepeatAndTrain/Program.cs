﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepeatAndTrain
{
    class Program
    {
        static void Main(string[] args)
        {
            //B obj1 = new A() as B;
            //obj1.Foo();

            B obj2 = new B();
            obj2.Foo();

            A obj3 = new B();
            obj3.Foo();
        }
    }
    class A
    {
        public virtual void Foo()
        {
            Console.WriteLine("Class A");
        }
    }
    class B : A
    {
        public void Foo()
        {
            Console.WriteLine("Class B");
        }
    }

    abstract class C : A
    {

    }
}
