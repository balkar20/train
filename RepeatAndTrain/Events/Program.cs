﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events
{
    
    class Account
    {
        // Объявляем делегат
        public delegate void AccountStateHandler(string message);
        // Событие, возникающее при выводе денег
        public event AccountStateHandler Withdrowed;
        // Событие, возникающее при добавление на счет
        public event AccountStateHandler Added;


        /// <summary>
        /// //////////////////////////////////////////////////////
        /// </summary>

        int _sum; // Переменная для хранения суммы
        int _percentage; // Переменная для хранения процента

        public Account(int sum, int percentage)
        {
            _sum = sum;
            _percentage = percentage;
        }

        public int CurrentSum
        {
            get { return _sum; }
        }

        public void Put(int sum)
        {
            _sum += sum;
            if (Added != null)
                Added("На счет поступило " + sum);
        }

        //public void Withdraw(int sum)
        //{
        //    if (sum <= _sum)
        //    {
        //        _sum -= sum;
        //    }
        //}
        public void Withdraw(int sum)
        {
            if (sum <= _sum)
            {
                _sum -= sum;
                if (Withdrowed != null)
                    Withdrowed("Сумма " + sum + " снята со счета");
            }
            else
            {
                if (Withdrowed != null)
                    Withdrowed("Недостаточно денег на счете");
            }
        }

        public int Percentage
        {
            get { return _percentage; }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Account account = new Account(200, 6);
            // Добавляем обработчики события
            account.Added += Show_Message;
            account.Withdrowed += Show_Message;

            account.Withdraw(100);
            // Удаляем обработчик события
            account.Withdrowed -= Show_Message;

            account.Withdraw(50);
            account.Put(150);

            Console.ReadLine();
        }
        private static void Show_Message(string message)
        {
            Console.WriteLine(message);
        }
    }
}
