﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace RegExp
{
    class Program
    {
        static void Main(string[] args)
        {
            string date = "Расчетный листок  за Июль 2017 г.";
            string name = "Николаенко Михаил Викторович оклад: 574.16";
            string name2 = " 574.16";

            string vegaNamePattern = @"(оклад:)\s[0-9]{1,3}(?:[.,][0-9]{1,3})?\z";
            string vegaPeriosPattern = @"^(Расчетный листок  за )";

            Regex rgx = new Regex(vegaNamePattern);
            Regex rgx2 = new Regex(vegaPeriosPattern);

            Console.WriteLine(Regex.IsMatch(name,vegaNamePattern));
            Console.WriteLine(Regex.IsMatch(name2,vegaNamePattern));

            Console.WriteLine();

            Console.WriteLine(Regex.IsMatch(date, vegaPeriosPattern));


            string resultPerios = rgx2.Replace(date, ""); 
            string resultName = rgx.Replace(name, "");

            Console.WriteLine("Результат имени : " + resultName);
            Console.WriteLine("Результат даты : " + resultPerios);



        }
    }
}
