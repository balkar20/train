﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates2
{
    class Account
    {
        // Объявляем делегат
        public delegate void AccountStateHandler(string message);
        // Создаем переменную делегата
        AccountStateHandler del;

        // Регистрируем делегат
        public void RegisterHandler(AccountStateHandler _del)
        {
            Delegate mainDel = System.Delegate.Combine(_del, del);
            del = mainDel as AccountStateHandler;
        }
        // Отмена регистрации делегата
        public void UnregisterHandler(AccountStateHandler _del)
        {
            Delegate mainDel = System.Delegate.Remove(del, _del);
            del = mainDel as AccountStateHandler;
        }


        /// <summary>
        /// //////////////////////////////////////////////////////
        /// </summary>

        int _sum; // Переменная для хранения суммы
        int _percentage; // Переменная для хранения процента

        public Account(int sum, int percentage)
        {
            _sum = sum;
            _percentage = percentage;
        }

        public int CurrentSum
        {
            get { return _sum; }
        }

        public void Put(int sum)
        {
            _sum += sum;
        }

        //public void Withdraw(int sum)
        //{
        //    if (sum <= _sum)
        //    {
        //        _sum -= sum;
        //    }
        //}
        public void Withdraw(int sum)
        {
            if (sum <= _sum)
            {
                _sum -= sum;

                if (del != null)
                    del("Сумма " + sum.ToString() + " снята со счета");
            }
            else
            {
                if (del != null)
                    del("Недостаточно денег на счете");
            }
        }

        public int Percentage
        {
            get { return _percentage; }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Account account = new Account(200, 6);
            Account.AccountStateHandler colorDelegate = new Account.AccountStateHandler(Color_Message);

            // Добавляем в делегат ссылку на методы
            account.RegisterHandler(new Account.AccountStateHandler(Show_Message));
            account.RegisterHandler(colorDelegate);
            // Два раза подряд пытаемся снять деньги
            account.Withdraw(100);
            account.Withdraw(150);

            // Удаляем делегат
            account.UnregisterHandler(colorDelegate);
            account.Withdraw(50);

            Console.ReadLine();
        }
        private static void Show_Message(String message)
        {
            Console.WriteLine(message);
        }
        private static void Color_Message(string message)
        {
            // Устанавливаем красный цвет символов
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            // Сбрасываем настройки цвета
            Console.ResetColor();
        }
    }
}
