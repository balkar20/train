import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }   from './app.component';
import { FactorialPipe} from './factorial.pipe';
 
@NgModule({
    imports:      [ BrowserModule],
    declarations: [ AppComponent, FactorialPipe],
    bootstrap:    [ AppComponent ]
})
export class AppModule { }