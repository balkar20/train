import { Component} from '@angular/core';
       
@Component({
    selector: 'my-app',
    template: `<div>Факториал числа {{x}} равен {{x | factorial}}</div>`
})
export class AppComponent { 
 
    x: number = 5;
}