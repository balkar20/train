import { Component} from '@angular/core';
 
@Component({
    selector: 'my-app',
    template: `<div class="special" [class.special]="isSpecial">
                  <p bold>Hello Angular 2</p>
                  <p>Angular 2 представляет модульную архитектуру приложения</p>
               </div>`
})
export class AppComponent {
    isSpecial: boolean = true;
}