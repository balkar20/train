import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-users-data',
  templateUrl: './users.html'
})
export class UsersComponent {
  public users: IUsers[];
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<IUsers[]>(baseUrl + 'api/User/GetUsers').subscribe(result => {
      this.users = result;
    }, error => console.error(error));
  }
 
}

interface IUsers {
  name: string;
  email: string;
  balackLevel: number;
}
