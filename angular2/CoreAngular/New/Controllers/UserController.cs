﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace New.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        [Route("[action]")]
        public IEnumerable<User> GetUsers()
        {
            return new User[]
            {
                new User() {Name = "VLad", Email = "balkar20@mail.ru", BlackLevel = 5},
                new User() {Name = "Slava", Email = "kolobok20@mail.ru", BlackLevel = 5}
            };
        }
    }

    public class User
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public int BlackLevel { get; set; }
    }
}