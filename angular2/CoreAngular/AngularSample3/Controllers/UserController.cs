﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AngularSample3.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;

namespace AngularSample3.Controllers
{
    [Route("api/[controller]")]
    public class UserController: ControllerBase
    {
        BlackContext db;

        protected UserController(BlackContext db)
        {
            this.db = this.db;
        }

        [HttpGet]
        public IEnumerable<User> Get()
        {
            return db.Users.ToList();
        }


        [HttpPut]
        public void UpdateUser(User user)
        {
            var Olduser = db.Users.Find(user.Id);

            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}
