﻿using AngularSample3.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace AngularSample3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DefaultController : ControllerBase
    {
        BlackContext db;

        public DefaultController(BlackContext db)
        {
            this.db = db;
        }

        [HttpGet]
        public IEnumerable<User> Get()
        {
            return new User[] { new User{Name = "Vasya", Email = "kjkj", BlackLevel = 4} , new User { Name = "Petya", Email = "hhh", BlackLevel = 4 }, };
        }

        // GET: api/Default/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Default
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Default/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
