﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './user';


@Injectable()
export class DataService {

    private url = "/api/default";

    constructor(private http: HttpClient) {
    }

    getUsers() {
        return this.http.get(this.url);
    }

    //getUser(id: number) {
    //    return this.http.get(this.url, id);
    //}

    createUser(user: User) {
        return this.http.post(this.url, user);
    }
    updateProduct(user: User) {

        return this.http.put(this.url + '/' + user.id, user);
    }
    deleteProduct(id: number) {
        return this.http.delete(this.url + '/' + id);
    }
}