var User = /** @class */ (function () {
    function User(id, name, blacklevel, role, email, photo) {
        this.id = id;
        this.name = name;
        this.blacklevel = blacklevel;
        this.role = role;
        this.email = email;
        this.photo = photo;
    }
    return User;
}());
export { User };
//# sourceMappingURL=user.js.map