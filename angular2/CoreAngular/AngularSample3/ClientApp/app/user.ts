﻿export class User {
    constructor(
        public id?: number,
        public name?: string,
        public blacklevel?: number,
        public role?: string,
        public email?: string,
        public photo?: number[]) { }
}