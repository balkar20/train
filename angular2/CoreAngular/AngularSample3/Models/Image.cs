﻿using System.ComponentModel.DataAnnotations;
using System.Data;

namespace AngularSample3.Models
{
    public class Image
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public byte[] Img { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}