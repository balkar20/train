﻿using AngularSample3.Models;
using Microsoft.EntityFrameworkCore;

namespace AngularSample3.Models
{
    public class BlackContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Image> Images { get; set; }

        public BlackContext(DbContextOptions<BlackContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}