﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AngularSample3.Models
{
    public class User
    {
        public int Id { get; set; }

        [MaxLength(30)]
        public string Name { get; set; }

        public int BlackLevel { get; set; }

        [MaxLength(100)]
        public string Email { get; set; }

        [Column(TypeName = "image")]
        public byte[] Photo { get; set; }

        [MaxLength(30)]
        public string Role { get; set; }
    }
}