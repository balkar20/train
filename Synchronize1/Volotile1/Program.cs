﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Volotile1
{
    class Program
    {
        // Далее вы увидите, что проблема решается объявлением этого поля volatile
        private static Boolean s_stopWorker = false;
        static void Main(string[] args)
        {
            Console.WriteLine("Main: letting worker run for 5 seconds");
            Thread t = new Thread(Worker);
            t.Start();
            Thread.Sleep(5000);
            s_stopWorker = true;
            Console.WriteLine("Main: waiting for worker to stop");
            t.Join();
        }

        private static void Worker(Object o)
        {
            Int32 x = 0;
            while (!s_stopWorker) x++;
            Console.WriteLine("Worker: stopped when x={0}", x);
        }
    }

    internal sealed class ThreadsSharingData
    {
        private Int32 m_flag = 0;
        private Int32 m_value = 0;
        // Этот метод выполняется одним потоком
        public void Thread1()
        {
            // ПРИМЕЧАНИЕ. 5 нужно записать в m_value до записи 1 в m_flag
            m_value = 5;
            Volatile.Write(ref m_flag, 1);
        }
        // Этот метод выполняется вторым потоком
        public void Thread2()
        {
            // ПРИМЕЧАНИЕ. Поле m_value должно быть прочитано после m_flag
            if (Volatile.Read(ref m_flag) == 1)
                Console.WriteLine(m_value);
        }
    }
}
