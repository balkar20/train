﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AvtoresetSamlpe1
{
    class Program
    {
        static void Main(string[] args)
        {

        }
    }

    internal sealed class SimpleWaitLock : IDisposable
    {
        private readonly AutoResetEvent m_available;
        public SimpleWaitLock()
        {
            m_available = new AutoResetEvent(true); // Изначально свободен
        }
        public void Enter()
        {
            // Блокирование на уровне ядра до освобождения ресурса
            m_available.WaitOne();
        }
        public void Leave()
        {
            // Позволяем другому потоку обратиться к ресурсу
            m_available.Set();
        }
        public void Dispose() { m_available.Dispose(); }
    }
}
