﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleSpinLockSample
{
    class Program
    {
        static void Main(string[] args)
        {

        }
    }

    internal struct SimpleSpinLock
    {
        private Int32 m_ResourceInUse; // 0=false (по умолчанию), 1=true

        public void Enter()
        {
            while (true)
            {
                // Всегда указывать, что ресурс используется.
                // Если поток переводит его из свободного состояния,
                // вернуть управление
                if (Interlocked.Exchange(ref m_ResourceInUse, 1) == 0) return;
                // Здесь что-то происходит...
            }
        }

        public void Leave()
        {
            // Помечаем ресурс, как свободный
            Volatile.Write(ref m_ResourceInUse, 0);
        }
    }
}
