﻿namespace BackGround1
{
    public class FindPrimesInput
    {
        public int To
        { get; set; }

        public int From
        { get; set; }

        public FindPrimesInput(int from, int to)
        {
            To = to;
            From = from;
        }

    }
}