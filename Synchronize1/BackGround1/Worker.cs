﻿using System.Collections.Generic;
using System.Threading;

namespace BackGround1
{
    public class Worker
    {
        public static int[] FindPrimes(int fromNumber, int toNumber)
        {
            // Найти простые числа в диапазоне между fromNumber 
            // и toNumber, вернув их в виде массива целых чисел
            List<int> list = new List<int>();
            for (int i = 0; i < 100; i++)
            {
                list.Add(i);
                Thread.Sleep(30);
            }

            return list.ToArray();
        }
    }
}