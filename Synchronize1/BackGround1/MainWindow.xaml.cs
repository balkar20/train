﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BackGround1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private BackgroundWorker backgroundWorker;
        public MainWindow()
        {
            InitializeComponent();
            backgroundWorker = ((BackgroundWorker) this.FindResource("backgroundWorker"));
            backgroundWorker.DoWork += BackgroundWorkerOnDoWork;
            backgroundWorker.RunWorkerCompleted+= BackgroundWorkerOnRunWorkerCompleted;
        }

        private void BackgroundWorkerOnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                // Ошибка была сгенерирована обработчиком события DoWork
                MessageBox.Show(e.Error.Message, "Произошла ошибка");
            }
            else
            {
                int[] primes = (int[])e.Result;
                foreach (int prime in primes)
                    lstPrimes.Items.Add(prime);
            }
            cmdFind.IsEnabled = true;
            cmdCancel.IsEnabled = false;
            progressBar.Value = 0;
        }

        private void BackgroundWorkerOnDoWork(object sender, DoWorkEventArgs  e)
        {
            // Получить входные значения
            FindPrimesInput input = (FindPrimesInput)e.Argument;

            // Запустить поиск простых чисел и ждать. 
            // Это длительная часть работы, но она не подвешивает 
            // пользовательский интерфейс, поскольку выполняется в другом потоке
            int[] primes = Worker.FindPrimes(input.From, input.To);

            // Вернуть результат
            e.Result = primes;
        }

        private void cmdFind_Click(object sender, RoutedEventArgs e)
        {
            // Сделать недоступной эту кнопку и очистить предыдущие результаты
            cmdFind.IsEnabled = false;
            cmdCancel.IsEnabled = true;
            lstPrimes.Items.Clear();

            // Получить диапазон поиска
            int from, to;
            if (!Int32.TryParse(txtFrom.Text, out from))
            {
                MessageBox.Show("Неверное значение начала диапазона");
                return;
            }
            if (!Int32.TryParse(txtTo.Text, out to))
            {
                MessageBox.Show("Неверное значение конца диапазона");
                return;
            }

            // Начать поиск простых чисел в другом потоке
            FindPrimesInput input = new FindPrimesInput(from, to);
            backgroundWorker.RunWorkerAsync(input);
        }

        private void cmdCancel_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
