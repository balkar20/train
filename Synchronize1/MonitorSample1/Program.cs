﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MonitorSample1
{
    internal sealed class Transaction
    {
        private readonly Object m_lock = new Object(); // Теперь блокирование                                      
                                                       // в рамках каждой транзакции ЗАКРЫТО
        private DateTime m_timeOfLastTrans;

        public void PerformTransaction()
        {
            Monitor.Enter(m_lock);// Вход в закрытую блокировку
            // Этот код имеет эксклюзивный доступ к данным...    
            m_timeOfLastTrans = DateTime.Now;
            Monitor.Exit(m_lock);// Выход из закрытой блокировки
        }

        public DateTime LastTransaction
        {
            get
            {
                Monitor.Enter(m_lock);// Вход в закрытую блокировку
                // Этот код имеет совместный доступ к данным...
                DateTime temp = m_timeOfLastTrans;
                Monitor.Exit(m_lock);// Завершаем закрытое блокирование
                return temp;
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
        }

        public static void SomeMethod()
        {
            var t = new Transaction();
    
            ThreadPool.QueueUserWorkItem(o => Console.WriteLine(t.LastTransaction));

        }
    }

}
