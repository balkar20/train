﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OneApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            Boolean createdNew;
            // Пытаемся создать объект ядра с указанным именем
            using (new Semaphore(0, 1, "SomeUniqueStringIdentifyingMyApp",
                out createdNew))
            {
                if (createdNew)
                {
                    // Этот поток создает ядро, так что другие копии приложения
                    // не могут запускаться. Выполняем остальную часть приложения...
                }
                else
                {
                    // Этот поток открывает существующее ядро с тем же именем;
                    // должна запуститься другая копия приложения.
                    // Ничего не делаем, ждем возвращения управления от метода Main,
                    // чтобы завершить вторую копию приложения
                }
            }
        }
    }
}
