﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace ParallelSample
{
    class MyClass
    {
        
    }
    class Program
    {
        static void Main(string[] args)
        {
            List<string> list = new List<string>{"one", "two", "three", "four"};

            Parallel.For(0, 4, i => DoWork(list[i]));

            Console.WriteLine("_____________________________________________________");

            Parallel.ForEach(list, i => Console.WriteLine("item -  {0}", i));

            Console.WriteLine("_____________________________________________________");

            Parallel.Invoke(() => OneMeth(), () => TwoMeth(), () => ThreMeth());
        }

        static void DoWork(string str)
        {
            Console.WriteLine("this is {0}", str);
        }

        static void OneMeth()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("one");
            }
        }

        static void TwoMeth()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("two");
            }
        }

        static void ThreMeth()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("three");
            }
        }
    }
}
