/**
 * Функция для генерации структуры иерархии
 * в виде дерева каталогоinstaв и файлов.
 */
 document.addEventListener("DOMContentLoaded", function(){

 })
 
function generateTree() {
    // var RESOURSES = document.getElementsByClassName("resource");
    // var OBJ_RESOURSES = parseResoursesToObjects(RESOURSES);

    var resourses = document.getElementsByClassName("resource");
    var res = resourses[0];

        // console.log("///////////////////////////////////////////");
        var objs = parseResoursesToObjects(resourses);
        // console.log(OBJ_RESOURSES.length);
        // console.log(OBJ_RESOURSES[61]);

        // console.log("///////////////////////////////////////////");
        // var groups = groupByDirectories(OBJ_RESOURSES);
        // console.log("group of elements:");
        // console.log(groups);

        // console.log("///////////////////////////////////////////");
        // var group = getResoursesByLocation('ROOT', OBJ_RESOURSES);
        // console.log(group);

        // console.log("///////////////////////////////////////////");
        var rootObj = getElementsWithLocation('ROOT', objs)[2];
    var firstEl = getHirarchy(objs, rootObj);
    console.log(firstEl);
    // console.log("///////////////////////////////////////////");
    // var group2 = getResoursesByLocation('bin', OBJ_RESOURSES);
    // console.log(group2);
}

var Resuorse = function(name, type, size, location) {
    this.name = name;
    this.type = type;
    this.size = size;
    this.location = location;

    this.getElementsWithThisLocation = function(objs) {
        var arr = [];
        for (var i = 0; i < objs.length; i++) {
            var res = objs[i];
            if (this.location == res.location) {
                arr.push(objs[i]);
            }
        }
        return arr;
    }
}

var Helper = function(ResObjs, rootName) {
    this.rootName = rootName;
    this.groups = groupByDirectories(ResObjs);

    function groupByDirectories(objs) {
        var groups = [];
        var arrHelp = objs;
        var cachedId = [];

        for (var i = 0; i < arrHelp.length; i++) {
            var el = arrHelp[i];
            var id = el.location;
            if (cachedId.indexOf(id) === -1) {
                var group = el.getElementsWithThisLocation(arrHelp);
                groups.push(group);
                cachedId.push(id);
            }
        }

        return groups;
    }

    function getGrouopLocation(group) {
        return group[0].location;
    };

    this.groupHierarchy = function(roots, groups) {
        var hir;
        var dirs = groups;
        //var arrHelp = roots;
        var subDirs = [];
        if (roots.length !== 0) {
            for (var i = 0; i < roots.length; i++) {
                for (var j = 0; j < dirs.length; j++) {
                    if (roots[i].location === getGrouopLocation(dirs[j])) {
                        var newRoot = dirs[j];
                        var newGroups = dirs.splice(j, 1);
                        hir = this.groupHierarchy(newRoot, newGroups);
                    }
                }
            }

        }
        return hir;
    };
}

function getResoursesByLocation(location, objs) {
    var groups = groupByDirectories(objs);
    var result;
    for (var i = 0; i < groups.length; i++) {
        if (groups[i][0].location === location) {
            result = groups[i];
        }
    }
    return result;
}


function parseResoursesToObjects(resourses) {
    var objs = [];
    for (var i = 0; i < resourses.length; i++) {
        objs.push(parseResToObject(resourses[i]));
    }
    return objs;
}

function getGrouopLocation(group) {
    return group[0].location;
}

function groupHierarchy(objs) {
    var groups = groupByDirectories(objs);
    var roots = getElementsWithLocation('ROOT', objs);
    if (objs === roots) {
        for (var i = 0; i < objs.length; i++) {
            objs[i]
        }
    } else if (objs !== roots) {

    }
}

function findFirstHirarchyElement(obj, objs) {
    var next;
    if (obj.location !== 'ROOT') {
        for (var i = 0; i < objs.length; i++) {
            if (objs[i].name == obj.location) {
                next = objs[i]
            }
        }

        next = findFirstHirarchyElement(next, objs);
    } else {
        return obj;
    }

    return next;
}

function buildHirarchy(){

}

function findLastHirarchyElements(rootObj, objs) {
    var groups = groupByDirectories(objs);
    var deep;
    var roots = getElementsWithLocation(rootObj.name, objs);
    if (roots.length !== 0) {
        for (var i = 0; i < roots.length; i++) {
            for (var j = 0; j < groups.length; j++) {
                if (groups[j][0].location == roots[i].name) {
                	console.log('find!: ');
                	console.log(groups[j][0]);
                	console.log(roots[i]);
                    deep = groups[j];
                }
            }

            deep = findLastHirarchyElements(deep, objs);
        }
    } else {
        return rootObj;
    }


    return deep;
}

function getHirarchy(arr, root){
    var deep = [];
    var parentName = root.name;
    for(var i in arr){
        var el = arr[i];
        if(arr[i].location == parentName) {
            var children = getHirarchy(arr, el.name);

            if(children.length > 0) {
                arr[i].children = children;
            }
            deep.push(arr[i]);
        }
    }
    return deep;
}

function pushElementOrArray(obj) {

    var hir = [];
    if (Array.isArray(obj)) {
        for (var i = 0; i < obj.length; i++) {
            hir.push(obj[i])
        }
    } else {
        hir.push(obj);
    }


    return hir;
}

function groupByDirectories(objs) {
    var groups = [];
    var arrHelp = objs;
    var cachedId = [];

    for (var i = 0; i < arrHelp.length; i++) {
        var id = arrHelp[i].location;
        if (cachedId.indexOf(id) === -1) {
            var group = arrHelp[i].getElementsWithThisLocation(arrHelp);
            groups.push(group);
            cachedId.push(id);
        }
    }

    return groups;
}

function deleteObjectsFromArray(arrFrom, arrOfDel) {
    var arr = arrFrom;
    for (var i = 0; i < arr.length; i++) {
        for (var j = 0; j < arrOfDel.length; j++) {
            if (arr[i].name === arrOfDel[j].name && arr[i].location === arrOfDel[j].location) {
                arr.splice(i, 1);
            }
        }
    }
    return arr;
}

function getElementsWithLocation(location, objs) {
    var arr = [];
    for (var i = 0; i < objs.length; i++) {
        var res = objs[i];
        if (location == res.location) {
            arr.push(objs[i]);
        }
    }
    return arr;
}

function getElementsWithNames(name, objs) {
    var arr = [];
    for (var i = 0; i < objs.length; i++) {
        var res = objs[i];
        if (name == res.name) {
            arr.push(objs[i]);
        }
    }
    return arr;
}

// function getElementsWithType(type,resourses){
// 	var arr = [];
// 	for (var i = 0; i < resourses.length; i++) {
// 		var dict = parseResToDictionary(resourses[i]);
// 		if(type == dict[1][0]){
// 			arr.push(resourses[i]);
// 		}
// 	}
// 	return arr;
// }

function parseResToDictionary(res) {
    var resTexts = getLiTexts(res);
    var keys = [];
    var values = []
    var dict;

    for (var i = 0; i < resTexts.length; i++) {
        var text = resTexts[i].replace(/ +/g, " ");
        var key = text.split(' ')[0];
        var val = text.split(' ')[1];
        keys.push(key);
        values.push(val);
    }
    dict = [keys, values]
    return dict;
}

function parseResToObject(res) {
    var name = getResourseName(res);
    var dict = parseResToDictionary(res);
    return new Resourse(name, dict[1][0], dict[1][1], dict[1][2]);
}

function getResourseName(res) {
    return res.firstElementChild.textContent;
}

function getLiTexts(resource) {
    var arr = [];
    var lies = resource.getElementsByTagName('li');
    for (var i = 0; i < lies.length; i++) {
        var two = lies[i].firstElementChild.textContent + lies[i].textContent.replace(lies[i].firstElementChild.textContent, '').replace(/\r|\n/g, '');
        arr.push(two);
    }
    return arr;
}


window.onload = generateTree();