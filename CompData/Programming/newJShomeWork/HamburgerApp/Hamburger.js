var Hamburger =  function(){
    return function(size, stuffing){
    this.size = size;
    this.stuffing = [];
    this.topping = [];
    
    
    if(stuffing !== null) {
        this.stuffing.push(stuffing);
    }

    
    this.addStuffing = function(stuffing) {
        if( size == Hamburger.SIZE_SMALL) {
            if(this.stuffing.length <= 5 && this.stuffing.indexOf(stuffing) === -1) {
                 this.stuffing.push(stuffing);
            }
            else{
                console.log('Нельзя добалять начинку к маленькому гамбургеру, если такая уже есть, и если уже добавлено 5 начинок!!!');
            }
        }
        else if(size == Hamburger.SIZE_LARGE) {
            if(this.stuffing <= 10 && this.stuffing.indexOf(stuffing) === -1) {
                this.stuffing.push(stuffing);
            }
            else{
                console.log("Нельзя добавлять начинку к большому гамбургеру, если уже добавленео 10 начинок !!!");
            }
        }
        else{
            console.log("Введены некоректные данные !!!");
        }
        
    };
    
    this.addTopping = function(topping) {
        if(this.topping.length <= 5 && this.topping.indexOf(topping) === -1) {
            this.topping.push(topping);
            if(this.stuffing.length + this.topping.length >= 5){
                this.size = Hamburger.SIZE_LARGE;
            }
            else{
                this.size = Hamburger.SIZE_SMALL;
            }
            
        }
        else{
            console.log("Нельзя добавлять более 5 топпингов и нельзя добавлять топпинг если такой уже есть !!!");
        }
    };
    
    this.remuveTopping = function(topping) {
        
      this.topping.forEach(function(el, i, arr) {
          if(arr.indexOf(topping) >= 0 ) {
             arr.splice(i, 1);
          }
      }) 
    };
    
    this.getSize = function() {
        
        return this.size;
        
    };
    
    this.getStuffing = function() {
        return this.stuffing;
    };
    
    this.getTopping = function() {
        return this.topping;
    };
    
    this.calculateCalories = function() {
        var result;
        var mainCalories = 200;
        var k = 33.6;
        var toppingCalor = k * (this.topping.length / 2);
        var stuffingCalor = k * this.stuffing.length;
        result = toppingCalor + stuffingCalor + mainCalories;
        return result;
            
    };
    
    this.calculatePrice = function() {
        var kSize;
        var result;
        var priceOfStuffing = this.stuffing.length * 0.1;
        var priceOfTopping = this.topping.length * 0.15;
        if(this.size === Hamburger.SIZE_LARGE)  {
            kSize = 1.5;
        }
        else if (this.size === Hamburger.SIZE_SMALL) {
            kSize = 0.7;
        }
        
        result = kSize + (priceOfStuffing + priceOfTopping);  
        
        return result;
    }
    
    
    
}
}();
var assortStuffing = [Hamburger.STUFFING_CHEESE = 'STUFFING_CHEESE',
Hamburger.STUFFING_MAYO = 'STUFFING_MAYO',
Hamburger.STUFFING_POTATO = 'STUFFING_POTATO',
Hamburger.STUFFING_SALAD = 'STUFFING_SALAD',
Hamburger.STUFFING_ONION = 'STUFFING_ONION',
Hamburger.STUFFING_PAPRICA = 'STUFFING_PAPRICA' ];

var assortTopping = [Hamburger.TOPPING_MAYO = 'TMayo',
Hamburger.TOPPING_SPICE = 'TOPPING_SPICE',
Hamburger.TOPPING_CHEESE = 'TOPPING_CHEESE',
Hamburger.TOPPING_POTATO = 'TOPPING_POTATO',
Hamburger.TOPPING_SALAD = 'TOPPING_SALAD',
Hamburger.TOPPING_ONION = 'TOPPING_ONION'];


var sizes = [Hamburger.SIZE_SMALL= 'SIZE_SMALL', Hamburger.SIZE_LARGE = 'SIZE_LARGE' ];
/*
Hamburger.SIZE_SMALL= 'small';
Hamburger.SIZE_LARGE = 'large';


// Начинки в ассортименте 
Hamburger.STUFFING_CHEESE = 'Cheese';
Hamburger.STUFFING_MAYO = 'Mayo';
Hamburger.STUFFING_POTATO = 'Potato';
Hamburger.STUFFING_SALAD = 'Salad';
Hamburger.STUFFING_ONION = 'Onion';
Hamburger.STUFFING_MAYO = 'Paprica';

// Топпинги в ассортименте 
Hamburger.TOPPING_MAYO = 'TMayo';
Hamburger.TOPPING_SPICE = 'TSpice';
Hamburger.TOPPING_CHEESE = 'TCheese';
Hamburger.TOPPING_POTATO = 'TPotato';
Hamburger.TOPPING_SALAD = 'TSalad';
Hamburger.TOPPING_ONION = 'TOnion';
*/


var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

console.log(Hamburger.STUFFING_CHEESE);
console.log(hamburger.stuffing);


hamburger.addStuffing(Hamburger.STUFFING_ONION);
hamburger.addStuffing(Hamburger.STUFFING_CHEESE);
console.log(hamburger.stuffing);

hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.addTopping(Hamburger.TOPPING_ONION);
hamburger.addTopping(Hamburger.TOPPING_MAYO);

console.log(hamburger.topping);

hamburger.remuveTopping(Hamburger.TOPPING_MAYO);

console.log(hamburger.topping);

var hamSIze = hamburger.getSize();
console.log("Размер гамбургера: " + hamSIze);

var hamStuffing = hamburger.getStuffing();

console.log("Начинки в гамбургере: " + hamStuffing);

var hamTopping = hamburger.getTopping();

console.log("Добавки в гамбургере: " + hamTopping);

var calories = hamburger.calculateCalories();

console.log("Калорийность: " + calories);

var price = hamburger.calculatePrice();

console.log("Цена: " + price + '$');