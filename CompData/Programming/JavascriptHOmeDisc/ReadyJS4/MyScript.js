//PROTOTYPE
var ObjectProto={
    name: "Sorax"
};
var object = Object.create(ObjectProto);
console.log(object.name);
//.....................

/*

Допустим нам нужно будет создать много объектов 
*/

var Person = {
    constructor: function(name,age, gender){
        this.name=name;
        this.age=age;
        this.gender=gender;
        return this;
    },
    greet: function(){
        console.log("Hi, my name is "+ this.name );
    }
};

var person, anotherPerson, thirdPerson;
person=Object.create(Person).constructor("John", 35, "male");
anotherPerson=Object.create(Person).constructor("Jessica", 45, "male");
thirdPerson=Object.create(Person).constructor("Bret", 25, "male");

console.log(person.name);
console.log(anotherPerson.name);
console.log(thirdPerson.name);

person.greet();
anotherPerson.greet();
thirdPerson.greet();

console.log(Person.isPrototypeOf(person));

// Создадим объект WebDeveloper

var WebDeveloper = Object.create(Person);
WebDeveloper.constructor = function(name, age, gender,skills){
    Person.constructor.apply(this, arguments);
    this.skills = skills || [];
    return this;
};
WebDeveloper.develop = function(){
  console.log("Working...");  
};

var developer = Object.create(WebDeveloper).constructor("Jack", 21 , "male",["php", "pyrhon", "java","js"]);
console.log(developer.skills);
developer.develop();
console.log(developer.name);
developer.greet();


