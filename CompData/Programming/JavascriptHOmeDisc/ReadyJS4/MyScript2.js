/*
Конструкторы и классы 
*/

var Person, person, anotherPerson;

Person = function(name){
    this.name =name; 
};

Person.prototype.greet = function(){
  console.log("Hello, my name is "  + this.name);  
};

person = new Person("Jack");
console.log(person.name);
person.greet();

//console.log(person.constructor);
console.log(Person.prototype.constructor);

anotherPerson= new Person("Bruse");
console.log(anotherPerson.name);
anotherPerson.greet();

console.log(anotherPerson instanceof Person);
console.log(Person.prototype.isPrototypeOf(anotherPerson));


console.log(Person.prototype)

console.log(anotherPerson._proto_);

/*
Создание дочерних классов 
*/

Developer = function(name, skills){
    Person.apply(this, arguments);
    this.skills=skills || [];
};

Developer.prototype = Object.create(Person.prototype);
Developer.prototype.constructor = Developer;
var developer;
developer = new Developer("John", ["ruby","for","python"]);
console.log(developer.name);
console.log(developer.skills);
developer.greet();
console.log(developer instanceof Developer);
console.log(developer instanceof Person);






