var students = [
    {
        name: 'John',
        age: 17,
        gender: 'M',
        grade: 8,
        state: 'Florida'
    },
    {
        name: 'Sarah',
        age: 19,
        gender: 'F',
        grade: 9,
        state: 'Alaska'
    },
    {
        name: 'Peter',
        age: 21,
        gender: 'M',
        grade: 5,
        state: 'California'
    },
    {
        name: 'Bred',
        age: 19,
        gender: 'M',
        grade: 8,
        state: 'Florida'
    },
    {
        name: 'Garry',
        age: 24,
        gender: 'M',
        grade: 9,
        state: 'Tennessee'
    },
    {
        name: 'Samantha',
        age: 14,
        gender: 'F',
        grade: 7,
        state: 'California'
    },
    {
        name: 'Garold',
        age: 16,
        gender: 'M',
        grade: 8,
        state: 'Washington'
    }
];
//Нахождение колличества совершеннолетних
var addults = [];
var adultsCount;
var i;
for(i=0;i<students.length;i++){
    if(students[i].age>=18)
        addults.push(students[i]);
}
adultsCount=addults.length;
console.log("adultsCount :"+adultsCount);
//...................................................................

//Средний балл всех учащихся
var meanGrade;
var sum=0;
for(var i=0;i<students.length;i++){
    sum+=students[i].grade;
}
meanGrade=sum/students.length;
console.log("meanGrad :"+meanGrade);
//...................................................................

//Средний балл среди несовершеннолетних
var teenMeanGrade;
var sumTeenGrade=0;
var studentsTeen=[];
for(var i=0;i<students.length;i++){
    if(students[i].age<18){
        studentsTeen.push(students[i]);
        sumTeenGrade+=students[i].grade;
    }
}
teenMeanGrade=sumTeenGrade/studentsTeen.length;
console.log("teenMeanGrade :"+teenMeanGrade);
//...................................................................

//Средний балл среди совершеннолетних парней
var menMeanGrade;
var sumMenMeanGrade=0;
var studMenMean=[];
for(var i =0;i<students.length;i++){
    if(students[i].age>=18 && students[i].gender=='M'){
        studMenMean.push(students[i]);
        sumMenMeanGrade+=students[i].grade;
    }
}
menMeanGrade=sumMenMeanGrade/studMenMean.length;
console.log("menMeanGrade :"+menMeanGrade);
//...................................................................

//Массив учащихся, отсортированных по возрастанию балов.
var studendsByGrades=students;
function compareNumeric(a,b){
    return a.age - b.age;
}
studendsByGrades.sort(compareNumeric);
console.log("studendsByGrades  :");
for(var i=0;i<studendsByGrades.length;i++){
    console.log(studendsByGrades[i].name);
}
//...................................................................

///Массив имен всех учащихся
var studentNames=[];
console.log("Первое имя из массива имен всех учащихся :");
for(var i=0;i<students.length;i++){
    studentNames.push(students[i].name);
}
console.log(studentNames[0]);
//...................................................................

//Массив имен всех девушек 
var girlNames=[];
console.log("Первое имя из массива имен всех учащихся Девушек:");
for(var i=0;i<students.length;i++){
    if(students[i].gender=='F'){
         girlNames.push(students[i].name);
    }
}
console.log(girlNames[0]);

//...................................................................
 
//Имена всех штатов в которых живут учащиеся (Без повторений)
var states=[];
console.log("Имена всех штатов в которых живут учащиеся :");
function in_array(value, array){
    for(var i=0;i<array.length;i++){
        if(array[i]==value) return true;
    }
    return false;
}
for(var i=0;i<students.length;i++){
    if(!(in_array(students[i].state,states)))
        states.push(students[i].state);
    }
for(var i=0;i<states.length;i++){
    console.log("Имя штата :"+states[i]);
}

//...................................................................

//Имена всех совершеннолетних из штата California
var californians=[];
console.log("Имена всех совершеннолетних из штата California : ");
for(var i=0;i<students.length;i++){
    if(students[i].age >=18 && students[i].state=='California')
    californians.push(students[i].name);
}
for(var i =0;i<californians.length;i++){
    console.log("Имя : " + californians[i]);
}
//...................................................................

//Средний балл учащихся из Аляски, с именем начинающимся на 'S'
var alaskaSMeanGread;
var str;

var ArrStud = [];
for(var i=0;i<students.length;i++){
    if(students[i].state=='Alaska'  && (students[i].name.search(/S/i))==0)
        ArrStud.push(students[i].grade);
}
console.log(ArrStud.length);
var sumGread =0;
for(var i =0;i<ArrStud.length;i++){
    sumGread+=ArrStud[i];
}
alaskaSMeanGread=sumGread/ArrStud.length;

console.log("Средний балл: " + alaskaSMeanGread);

//...................................................................