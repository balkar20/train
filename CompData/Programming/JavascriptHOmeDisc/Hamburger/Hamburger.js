var Hamburger =  function(){
    var hamburger = function(size, stuffing){
    this.size = size;
    this.stuffing = [];
    this.topping = [];
    
    
    if(stuffing !== null) {
        this.stuffing.push(stuffing.name);
    }

    
    this.addStuffing = function(stuffing) {
        if( size == Hamburger.SIZE_SMALL) {
            if(this.stuffing.indexOf(stuffing.name) < 0) {
                 this.stuffing.push(stuffing.name);
            }
            else{
                console.log('Нельзя добалять начинку к маленькому гамбургеру, если такая уже есть, и если уже добавлено 5 начинок!!!');
            }
        }
        else if(size == Hamburger.SIZE_LARGE) {
            if(this.stuffing.indexOf(stuffing.name) < 0) {
                this.stuffing.push(stuffing.name);
            }
            else{
                console.log("Нельзя добавлять начинку к большому гамбургеру, если уже добавленео 10 начинок !!!");
            }
        }
        else{
            console.log("Введены некоректные данные !!!");
        }
        
    };
    
    this.addTopping = function(topping) {
        if(this.topping.indexOf(topping.name) === -1) {
            this.topping.push(topping.name);
            if(this.stuffing.length + this.topping.length >= 5){
                this.size = Hamburger.SIZE_LARGE;
            }
            else{
                this.size = Hamburger.SIZE_SMALL;
            }
            
        }
        else{
            console.log("Нельзя добавлять более 5 топпингов и нельзя добавлять топпинг если такой уже есть !!!");
        }
    };
    
    this.remuveTopping = function(topping) {
        
      this.topping.forEach(function(el, i, arr) {
          if(arr.indexOf(topping) >= 0 ) {
             arr.splice(i, 1);
          }
      }) 
    };
    
    this.getSize = function() {
        
        return this.size;
        
    };
    
    this.getStuffing = function() {
        return this.stuffing;
    };
    
    this.getTopping = function() {
        return this.topping;
    };
    
    this.calculateCalories = function() {
        var result;
        var mainCalories = 200;
        var k = 33.6;
        var toppingCalor = k * (this.topping.length / 2);
        var stuffingCalor = k * this.stuffing.length;
        result = toppingCalor + stuffingCalor + mainCalories;
        return result;
            
    };
    
    this.calculatePrice = function() {
        var kSize;
        var result;
        var priceOfStuffing = this.stuffing.length * 0.1;
        var priceOfTopping = this.topping.length * 0.15;
        if(this.size === Hamburger.SIZE_LARGE)  {
            kSize = 1.5;
        }
        else if (this.size === Hamburger.SIZE_SMALL) {
            kSize = 0.7;
        }
        
        result = kSize + (priceOfStuffing + priceOfTopping);  
        
        return result;
    }
    
    
    
}
    return hamburger;
}();
// Начинки в ассортименте 

/*
var assortStuffing = [Hamburger.STUFFING_CHEESE = 'STUFFING_CHEESE',
Hamburger.STUFFING_MAYO = 'STUFFING_MAYO',
Hamburger.STUFFING_POTATO = 'STUFFING_POTATO',
Hamburger.STUFFING_SALAD = 'STUFFING_SALAD',
Hamburger.STUFFING_ONION = 'STUFFING_ONION',
Hamburger.STUFFING_PAPRICA = 'STUFFING_PAPRICA' ];

// Топпинги в ассортименте 
var assortTopping = [Hamburger.TOPPING_MAYO = 'TMayo',
Hamburger.TOPPING_SPICE = 'TOPPING_SPICE',
Hamburger.TOPPING_CHEESE = 'TOPPING_CHEESE',
Hamburger.TOPPING_POTATO = 'TOPPING_POTATO',
Hamburger.TOPPING_SALAD = 'TOPPING_SALAD',
Hamburger.TOPPING_ONION = 'TOPPING_ONION'];
*/

var Assorty = (function(){ Hamburger.STUFFING_CHEESE = {name: 'STUFFING_CHEESE', calories: 25, Price:  0.1};
Hamburger.STUFFING_MAYO = {name: 'STUFFING_MAYO', calories: 23, Price: 0.15};
Hamburger.STUFFING_POTATO = {name: 'STUFFING_POTATO', calories: 27, Price: 0.2};
Hamburger.STUFFING_SALAD = {name: 'STUFFING_SALAD', calories: 23, Price: 0.1};
Hamburger.STUFFING_ONION = {name: 'STUFFING_ONION', calories: 23, Price: 0.1};
Hamburger.STUFFING_MAYO = {name: 'STUFFING_MAYO', calories: 28, Price: 0.3};
                          
Hamburger.TOPPING_MAYO = {name: 'TOPPING_MAYO', calories: 21, Price: 0.2};
Hamburger.TOPPING_SPICE = {name: 'TOPPING_SPICE', calories: 24, Price: 0.3};
Hamburger.TOPPING_CHEESE = {name: 'TOPPING_CHEESE', calories: 21, Price: 0.1};
Hamburger.TOPPING_POTATO = {name: 'TOPPING_POTATO', calories: 22, Price: 0.3};
Hamburger.TOPPING_SALAD = {name: 'TOPPING_SALAD', calories: 21, Price: 0.2};
Hamburger.TOPPING_ONION = {name: 'TOPPING_ONION', calories: 23, Price: 0.1};    
                          
Hamburger.SIZE_SMALL = { name: "SIZE_SMALL", maxSize: 5, startprice: 1.5};  
Hamburger.SIZE_LARGE = { name: "SIZE_LARGE", maxSize: 10, startprice: 0.75};
                          
})();


var sizes = [Hamburger.SIZE_SMALL, Hamburger.SIZE_LARGE]; //Размеры 



// Начинки в ассортименте |
var stuffings = [Hamburger.STUFFING_CHEESE, Hamburger.STUFFING_MAYO, Hamburger.STUFFING_POTATO, Hamburger.STUFFING_SALAD, Hamburger.STUFFING_ONION, Hamburger.STUFFING_MAYO];



// Топпинги в ассортименте 
var toppings = [Hamburger.TOPPING_MAYO, Hamburger.TOPPING_SPICE, Hamburger.TOPPING_CHEESE, Hamburger.TOPPING_POTATO, Hamburger.TOPPING_SALAD, Hamburger.TOPPING_ONION ]


var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.TOPPING_SALAD);

console.log(Hamburger.STUFFING_CHEESE);
console.log(hamburger.stuffing);


hamburger.addStuffing(Hamburger.STUFFING_ONION);
hamburger.addStuffing(Hamburger.STUFFING_CHEESE);
console.log(hamburger.stuffing);

hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.addTopping(Hamburger.TOPPING_ONION);
hamburger.addTopping(Hamburger.TOPPING_MAYO);

console.log(hamburger.topping);

hamburger.remuveTopping(Hamburger.TOPPING_MAYO);

console.log(hamburger.topping);

var hamSIze = hamburger.getSize();
console.log("Размер гамбургера: " + hamSIze);

var hamStuffing = hamburger.getStuffing();

console.log("Начинки в гамбургере: " + hamStuffing);

var hamTopping = hamburger.getTopping();

console.log("Добавки в гамбургере: " + hamTopping);

var calories = hamburger.calculateCalories();

console.log("Калорийность: " + calories);

var price = hamburger.calculatePrice();

console.log("Цена: " + price + '$');