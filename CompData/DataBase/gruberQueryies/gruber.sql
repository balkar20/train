USE Gruber;

CREATE TABLE SELLERS(
        SNUM int not null,
        SNAME varchar(20) not null,
        CITY varchar(20),
        COMM numeric(4,2),
        PRIMARY KEY (SNUM)
    );

CREATE TABLE CUSTOMERS(
        CNUM int not null,
        CNAME varchar(20) not null,
        CITY varchar(20),
        RATING numeric(4),
        SNUM int not null,
        PRIMARY KEY (CNUM)
    );

CREATE TABLE ORDERS(
        ONUM int not null,
        AMT numeric(7,2) not null,
        ODATE datetime not null,
        CNUM int not null,
        SNUM int not null,
        PRIMARY KEY (ONUM)
    );
    
ALTER TABLE CUSTOMERS 
    ADD CONSTRAINT CUSTOMERS_SELLERS_FK 
    FOREIGN KEY (SNUM) 
    REFERENCES SELLERS;

ALTER TABLE ORDERS 
    ADD CONSTRAINT ORDERS_CUSTOMERS_FK 
    FOREIGN KEY (CNUM) 
    REFERENCES CUSTOMERS;

ALTER TABLE ORDERS 
    ADD CONSTRAINT ORDERS_SELLERS_FK 
    FOREIGN KEY (SNUM) 
    REFERENCES SELLERS;

TRUNCATE TABLE ORDERS;
DELETE FROM SELLERS;
DELETE FROM CUSTOMERS;

INSERT INTO SELLERS (SNUM, SNAME, CITY, COMM) VALUES (1001, 'Peel', 'London',  0.12);
INSERT INTO SELLERS (SNUM, SNAME, CITY, COMM) VALUES (1002, 'Serres', 'San Jose',  0.13);
INSERT INTO SELLERS (SNUM, SNAME, CITY, COMM) VALUES (1004, 'Motika', 'London',  0.11);
INSERT INTO SELLERS (SNUM, SNAME, CITY, COMM) VALUES (1007, 'Rifkin', 'Barcelona',  0.15);
INSERT INTO SELLERS (SNUM, SNAME, CITY, COMM) VALUES (1003, 'Axelrod', 'New York',  0.10);

INSERT INTO CUSTOMERS (CNUM, CNAME, CITY, RATING, SNUM) VALUES (2001, 'Hoffman', 'London', 100, 1001);
INSERT INTO CUSTOMERS (CNUM, CNAME, CITY, RATING, SNUM) VALUES (2002, 'Giovanni', 'Rome', 200, 1003);
INSERT INTO CUSTOMERS (CNUM, CNAME, CITY, RATING, SNUM) VALUES (2003, 'Liu', 'SanJose', 200, 1002);
INSERT INTO CUSTOMERS (CNUM, CNAME, CITY, RATING, SNUM) VALUES (2004, 'Grass', 'Berlin', 300, 1002);
INSERT INTO CUSTOMERS (CNUM, CNAME, CITY, RATING, SNUM) VALUES (2006, 'Clemens', 'London', 100, 1001);
INSERT INTO CUSTOMERS (CNUM, CNAME, CITY, RATING, SNUM) VALUES (2008, 'Cisneros', 'SanJose', 300, 1007);
INSERT INTO CUSTOMERS (CNUM, CNAME, CITY, RATING, SNUM) VALUES (2007, 'Pereira', 'Rome', 100, 1004);

INSERT INTO ORDERS (ONUM, AMT, ODATE, CNUM, SNUM) VALUES (3001, 18.69, Convert(Datetime,'1990-03-10', 120), 2008, 1007);
INSERT INTO ORDERS (ONUM, AMT, ODATE, CNUM, SNUM) VALUES (3003, 767.19, Convert(Datetime,'1990-03-10', 120), 2001, 1001);
INSERT INTO ORDERS (ONUM, AMT, ODATE, CNUM, SNUM) VALUES (3002, 1900.10, Convert(Datetime,'1990-03-10', 120), 2007, 1004);
INSERT INTO ORDERS (ONUM, AMT, ODATE, CNUM, SNUM) VALUES (3005, 5160.45, Convert(Datetime,'1990-03-10', 120), 2003, 1002);
INSERT INTO ORDERS (ONUM, AMT, ODATE, CNUM, SNUM) VALUES (3006, 1098.16, Convert(Datetime,'1990-03-10', 120), 2008, 1007);
INSERT INTO ORDERS (ONUM, AMT, ODATE, CNUM, SNUM) VALUES (3009, 1713.23, Convert(Datetime,'1990-04-10', 120), 2002, 1003);
INSERT INTO ORDERS (ONUM, AMT, ODATE, CNUM, SNUM) VALUES (3007, 75.75, Convert(Datetime,'1990-04-10', 120), 2004, 1002);
INSERT INTO ORDERS (ONUM, AMT, ODATE, CNUM, SNUM) VALUES (3008, 4723.00, Convert(Datetime,'1990-05-10', 120), 2006, 1001);
INSERT INTO ORDERS (ONUM, AMT, ODATE, CNUM, SNUM) VALUES (3010, 1309.95, Convert(Datetime,'1990-06-10', 120), 2004, 1002);
INSERT INTO ORDERS (ONUM, AMT, ODATE, CNUM, SNUM) VALUES (3011, 9891.88, Convert(Datetime,'1990-06-10', 120), 2006, 1001);