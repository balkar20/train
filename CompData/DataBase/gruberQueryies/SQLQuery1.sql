use Gruber
-- Distinct

SELECT DISTINCT [RATING]
  FROM [Gruber].[dbo].[CUSTOMERS];


SELECT ALL [RATING]
  FROM [Gruber].[dbo].[CUSTOMERS];


--������ ���������

SELECT *
FROM Customers
WHERE rating > 200;


SELECT *
FROM Customers
WHERE city = 'SanJose'
AND rating > 200;


SELECT *
FROM Customers
WHERE city = 'SanJose'
OR NOT rating > 200;


-- 3 steps
SELECT *
FROM Orders
WHERE  (odate = 10/03/1990 AND snum >1002);

SELECT *
FROM Orders
WHERE  (odate = 10/03/1990 AND snum >1002) OR amt > 2000.00;

SELECT *
FROM Orders
WHERE NOT ((odate = 10/03/1990 AND snum >1002) OR amt > 2000.00);

-- ������������� ����������� ���������� � ��������

SELECT *
FROM SELLERS
WHERE city = 'Barcelona' OR city = 'London';

SELECT *
FROM SELLERS
WHERE city IN ( 'Barcelona', 'London' );

SELECT *
FROM Customers
WHERE cnum IN ( 2001, 2007, 2004 );

SELECT *
FROM SELLERS
WHERE comm BETWEEN .10 AND .12;

SELECT *
FROM SELLERS
WHERE (comm BETWEEN .10 AND .12) AND NOT comm IN (.10, .12);

SELECT *
FROM Customers
WHERE cname LIKE 'G%';

SELECT *
FROM SELLERS
WHERE sname LIKE 'P__l%';

-- NULL

SELECT *
FROM Customers
WHERE CITY IS NOT NULL;

SELECT *
FROM Customers
WHERE NOT city IS NULL;

--���������� �������


SELECT SUM ((amt))
FROM Orders;


SELECT AVG (amt)
FROM Orders;

SELECT COUNT ( DISTINCT snum )
FROM Orders;

SELECT COUNT ( ALL rating )
FROM Customers;

SELECT MAX ( 10 + (amt) )
FROM Orders;

-- GROUP BY

SELECT snum, MAX (amt)
FROM Orders
GROUP BY snum;


SELECT snum, odate, MAX ((amt))
FROM Orders
GROUP BY snum, odate;

---HAVING

SELECT snum, odate, MAX ((amt))
FROM Orders
GROUP BY snum, odate
HAVING MAX ((amt)) > 3000.00;

-- ������������ ������� ��������

SELECT snum, sname, city, comm * 100
FROM SELLERS;

SELECT snum, sname, city, ' % ', comm * 100
FROM SELLERS;

SELECT *
FROM Orders
ORDER BY cnum DESC;

SELECT *
FROM Orders
ORDER BY cnum ASC, amt DESC;

SELECT snum, odate, MAX (amt)
FROM Orders
GROUP BY snum, odate
ORDER BY snum;

SELECT sname, comm
FROM SELLERS
ORDER BY 2 DESC;

--����������� ������

SELECT Customers.cname, SELLERS.sname, SELLERS.city
FROM SELLERS, Customers
WHERE SELLERS.city = Customers.city;

SELECT sname, cname
FROM SELLERS, Customers
WHERE sname < cname AND rating < 200;

SELECT onum, cname, Orders.cnum, Orders.snum
FROM SELLERS, Customers,Orders
WHERE Customers.city < > SELLERS.city
AND Orders.cnum = Customers.cnum
AND Orders.snum = SELLERS.snum;

-- ����������� ������� � �����

SELECT first.cname, second.cname, first.rating
FROM Customers first, Customers second
WHERE first.rating = second.rating;

SELECT first.cname, second.cname, first.rating
FROM Customers first, Customers second
WHERE first.rating = second.rating
AND first.cname < second.cname;

--����������

SELECT *
FROM Orders
WHERE snum = (SELECT snum
FROM SELLERS
WHERE sname = 'Motika');

SELECT *
FROM Orders
WHERE snum = ( SELECT snum
FROM SELLERS
WHERE city = 'Barcelona' );

SELECT *
FROM Orders
WHERE snum = ( SELECT DISTINCT snum
FROM Orders
WHERE cnum = 2001 );

-- EXIST

SELECT cnum, cname, city
FROM Customers
WHERE EXISTS ( SELECT *
FROM Customers
WHERE city = 'SanJose' );

SELECT DISTINCT snum
FROM Customers outer1
WHERE NOT EXISTS ( SELECT *
FROM Customers inner1
WHERE inner1.snum = outer1.snum
AND inner1.cnum <> outer1.cnum );


-- SOME
SELECT *
FROM SELLERS
WHERE city = ANY (SELECT city
FROM Customers );


-- JOIN

SELECT * from SELLERS S
INNER JOIN ORDERS A
ON S.SNUM=A.SNUM

SELECT * from SELLERS S 
FULL OUTER JOIN ORDERS A 
ON S.SNUM=A.SNUM

SELECT * from SELLERS S 
LEFT OUTER JOIN ORDERS A 
ON S.SNUM=A.SNUM

SELECT * from SELLERS S 
RIGHT OUTER JOIN ORDERS A 
ON S.SNUM=A.SNUM
