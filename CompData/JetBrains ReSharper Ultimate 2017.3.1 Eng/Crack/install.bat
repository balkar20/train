
@echo off
setlocal
cd /d %~dp0

echo Check operating system ...
if defined PROGRAMFILES(X86) (
    echo 64-bit sytem detected
	start dvt-jb_licsrv.amd64.exe -mode install
) else (
    echo 32-bit sytem detected
	start dvt-jb_licsrv.386.exe -mode install
)

net start JetBrainsLicServerDVT
pause
