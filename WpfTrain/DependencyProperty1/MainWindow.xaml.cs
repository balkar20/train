﻿using System.Windows;

namespace DependencyProperty1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button1_OnClick(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(myFirstControl1.Data.ToString());
        }
    }
}
