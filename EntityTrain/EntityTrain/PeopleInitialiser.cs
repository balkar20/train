﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace EntityTrain
{
    public class PeopleInitialiser : DropCreateDatabaseIfModelChanges<PeopleContext>
    {
        protected override void Seed(PeopleContext context)
        {
            context.People.AddRange(new List<People> {new People { }});
        }
    }
}