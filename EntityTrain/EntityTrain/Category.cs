﻿using System.Collections.Generic;

namespace EntityTrain
{
    public class Category
    {
        public Category()
        {
            Peoples = new List<People>();
        }

        public int Id { get; set; }
        public string CatName { get; set; }
        
        public List<People> Peoples { get; set; }
        
    }
}