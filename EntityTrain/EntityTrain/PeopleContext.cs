﻿using System.Data.Entity;

namespace EntityTrain
{
    public class PeopleContext : DbContext
    {
        protected PeopleContext():base("PeopleContext")
        {
        }

        public DbSet<People> People { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}