﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqTrain
{
    class Program
    {

        static void Main(string[] args)
        {
            //Dictionary<int, Employee> eDictionary =
            //    Employee.GetEmployeesArray().ToDictionary(k => k.id);
            //Employee e = eDictionary[2];
            //Console.WriteLine("Сотрудником с id == 2 является " + e.firstName + " " + e.lastName);

            Dictionary<string, Employee2> e2Dictionary =
                Employee2.GetEmployeesArray().ToDictionary(k => k.id, new MyStringifiedNumberComparer());
            Employee2 e2 = e2Dictionary["2"];
            Console.WriteLine("Сотрудником с id == \"2\" является " + e2.firstName + " " + e2.lastName);

            e2 = e2Dictionary["00002"];
            Console.WriteLine("Сотрудником с id == \"00002\" является " + e2.firstName + " " + e2.lastName);
        }
    }
}
