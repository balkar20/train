﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqTrain1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Операция Aggregate\n\n********\n");

            int agg = Enumerable
                .Range(1, 10)
                .Aggregate(0, (s, i) => s + i);
            Console.WriteLine("Сумма = " + agg);

            Console.ReadKey();
        }
    }
}
